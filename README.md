# Packer Vagrant Ubuntu Jumpbox build

Packer scripts to build up a Jumpbox.

## Tools to build the vagrant image
- [Packer](https://www.packer.io/)
- [Vagrant](https://www.vagrantup.com/)
- [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

## Thanks
Built by leveraging off some work done by a few other people so thanks to these guys:
- kaorimatz for [packer-templates](https://github.com/kaorimatz/packer-templates)
- dwallraff for [jumpbox-install.sh](https://gist.github.com/dwallraff/c195e082b268ad56453fdab332a568db) and [dotfiles](https://github.com/dwallraff/dotfiles)
- RamXX for [cfjump](https://github.com/RamXX/cfjump)

## Usage
Clone the repository:
```
git clone https://github.com/kaorimatz/packer-templates && cd packer-templates
```

Build a machine image from the template in the repository:
```
packer build ubuntu-v17.10-jumpbox.json
```

Add the built box to Vagrant:
```
vagrant box add ubuntu-v17.10-jumpbox.box --name ubuntu/jumpbox
```
