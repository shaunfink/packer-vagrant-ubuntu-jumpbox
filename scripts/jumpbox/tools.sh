#!/bin/bash
# Blatantly stolen from @RamXX. It's cool, we're friends.

# Script will install a plethora of tools for working with
# PCF deployments and a variety of IaaS's

# Tweaked for Ubuntu 16.04

######
## Setup
######

# Add other repos
sudo add-apt-repository -y ppa:ubuntu-lxc/lxc-stable

######
## Basic Tools
######

# Basic OS
sudo apt-get update -y
sudo apt-get upgrade -y

# Basic Tools and languages

OS_TOOLS=(\
apt-transport-https \
build-essential \
curl \
dnsutils \
git \
hping3 \
iperf \
iputils-ping \
jq \
less \
libdap-bin \
mongodb-clients \
mysql-client \
netcat \
nmap \
nodejs \
npm \
python \
python-dev \
python-pip \
python-setuptools \
python3-pip \
python3-venv \
redis-tools \
ruby \
s3cmd \
s3curl \
screen \
software-properties-common \
tcpdump \
tmate \
tmux \
traceroute \
unzip \
vim \
wget \
gcc \
linux-headers-generic \
libssl-dev \
libffi-dev \
gnupg2 \
git-crypt \
ruby-dev \
postgresql-client
)

sudo apt-get install -y "${OS_TOOLS[@]}"

######
## IaaS Tools
######

# GCP cli
export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - &&
sudo apt-get update -y && sudo apt-get install -y google-cloud-sdk

# # AWS cli
python3 -m pip install --upgrade pip
python3 -m pip install --upgrade awscli --user

# AZURE cli
export AZ_REPO=$(lsb_release -c -s)
echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" | sudo tee /etc/apt/sources.list.d/azure-cli.list &&
sudo apt-key adv --keyserver packages.microsoft.com --recv-keys 52E16F86FEE04B979B07E28DB02C46DF417A0893 &&
curl -L https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add - &&
sudo apt-get update && sudo apt-get install -y azure-cli

# Photon
# Thanks to Merlin Glynn (@virtmerlin) for this part!
cd /usr/local/bin && sudo wget -O photon \
    "$(curl -s https://api.github.com/repos/vmware/photon-controller/releases/latest \
    | jq --raw-output '.assets[] | .browser_download_url' | grep linux | grep -v sum)" && sudo chmod +x photon

# Terraform
cd /usr/local/bin && sudo wget -O terraform \
  "$(curl -s https://www.terraform.io/downloads.html | grep -o \".*linux_amd64.*zip.*\" | jq -r '.')" &&
  sudo unzip -o terraform && sudo chmod +x terraform

# cliaas
cd /usr/local/bin && sudo wget -O cliaas \
    "$(curl -s https://api.github.com/repos/pivotal-cf/cliaas/releases/latest \
    | jq --raw-output '.assets[] | .browser_download_url' | grep linux)" && sudo chmod +x cliaas

######
## BOSH Tools
######

# bosh
cd /usr/local/bin && sudo wget -O bosh \
  "$(curl -s https://bosh.io/docs/cli-v2/#install | grep -o \".*linux-amd64\" | jq -r '.')" && sudo chmod +x bosh

# certstrap
cd /usr/local/bin && sudo wget -O certstrap \
    "$(curl -s https://api.github.com/repos/square/certstrap/releases/latest \
    | jq --raw-output '.assets[] | .browser_download_url' | grep linux | grep -v zip)" && sudo chmod +x certstrap

# spiff
cd /usr/local/bin && sudo wget -O spiff \
    "$(curl -s https://api.github.com/repos/cloudfoundry-incubator/spiff/releases/latest \
    | jq --raw-output '.assets[] | .browser_download_url' | grep linux)" && sudo unzip -o spiff && sudo sudo chmod +x spiff

# spruce
cd /usr/local/bin && sudo wget -O spruce \
    "$(curl -s https://api.github.com/repos/geofffranks/spruce/releases/latest \
    | jq --raw-output '.assets[] | .browser_download_url' | grep linux | grep -v zip)" && sudo chmod +x spruce

# om
echo "deb http://apt.starkandwayne.com stable main" | sudo tee /etc/apt/sources.list.d/starkandwayne.list &&
curl https://raw.githubusercontent.com/starkandwayne/homebrew-cf/master/public.key | sudo apt-key add - &&
sudo apt-get update -y && sudo apt-get install -y om

######
## CloudFoundry Tools
######

# cf cli
echo "deb https://packages.cloudfoundry.org/debian stable main" | sudo tee /etc/apt/sources.list.d/cloudfoundry-cli.list &&
curl https://packages.cloudfoundry.org/debian/cli.cloudfoundry.org.key | sudo apt-key add - &&
sudo apt-get update && sudo apt-get install -y cf-cli
# https://plugins.cloudfoundry.org/?

# uaac cli
sudo gem install cf-uaac --no-rdoc --no-ri

# bbr
cd /usr/local/bin && sudo wget -O bbr \
    "$(curl -s https://api.github.com/repos/cloudfoundry-incubator/bosh-backup-and-restore/releases/latest \
    | jq --raw-output '.assets[] | .browser_download_url')" && sudo tar xf bbr releases/bbr --strip-components 1 \
    && sudo chmod +x bbr

# asg creator
cd /usr/local/bin && sudo wget -O asg-creator \
    "$(curl -s https://api.github.com/repos/cloudfoundry-incubator/asg-creator/releases/latest \
    | jq --raw-output '.assets[] | .browser_download_url' | grep linux)" && sudo chmod +x asg-creator

# cf-mgmt
cd /usr/local/bin && sudo wget -O cf-mgmt \
    "$(curl -s https://api.github.com/repos/pivotalservices/cf-mgmt/releases/latest \
    | jq --raw-output '.assets[] | .browser_download_url' | grep linux | grep -v config)" && sudo chmod +x cf-mgmt
cd /usr/local/bin && sudo wget -O cf-mgmt-config \
    "$(curl -s https://api.github.com/repos/pivotalservices/cf-mgmt/releases/latest \
    | jq --raw-output '.assets[] | .browser_download_url' | grep linux | grep config)" && sudo chmod +x cf-mgmt-config

# pivnet cli
cd /usr/local/bin && sudo wget -O pivnet \
    "$(curl -s https://api.github.com/repos/pivotal-cf/pivnet-cli/releases/latest \
    | jq --raw-output '.assets[] | .browser_download_url' | grep linux)" && sudo chmod +x pivnet

######
## Other Tools
######

# vault
cd /usr/local/bin && sudo wget -O vault \
  "$(curl -s https://www.vaultproject.io/downloads.html | grep -o \".*linux_amd64.*zip.*\" | jq -r '.')" &&
  sudo unzip -o vault && sudo chmod +x vault

# credhub
cd /usr/local/bin && sudo wget -O credhub \
    "$(curl -s https://api.github.com/repos/cloudfoundry-incubator/credhub-cli/releases/latest \
    | jq --raw-output '.assets[] | .browser_download_url' | grep linux)" && sudo tar xzf credhub \
    && sudo chmod +x credhub

# fly cli
cd /usr/local/bin && sudo wget -O fly \
    "$(curl -s https://api.github.com/repos/concourse/fly/releases/latest \
    | jq --raw-output '.assets[] | .browser_download_url' | grep linux)" && sudo chmod +x fly

# safe
cd /usr/local/bin && sudo wget -O safe \
    "$(curl -s https://api.github.com/repos/starkandwayne/safe/releases/latest \
    | jq --raw-output '.assets[] | .browser_download_url' | grep linux)" && sudo chmod +x safe

# yaml-patch
cd /usr/local/bin && sudo wget -O yaml-patch \
    "$(curl -s https://api.github.com/repos/krishicks/yaml-patch/releases/latest \
    | jq --raw-output '.assets[] | .browser_download_url' | grep linux)" && sudo chmod +x yaml-patch
